import React, { useState } from 'react';
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Article from "./pages/Article";
import "./App.css";
import { 
  Switch, 
  Route
} from "react-router-dom";
import {
  Navbar,
  NavbarBrand,
  NavLink,
  Nav
} from 'reactstrap';

const Components = () => {
  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/home/"><h1 className="text-dark">My Site</h1></NavbarBrand>
        <Nav className="mr-auto">
          <NavLink href="/home/">Home</NavLink>
          <NavLink href="/about/">About</NavLink>
          <NavLink href="/contact/">Contact</NavLink>
          <NavLink href="/article/">Article</NavLink>
        </Nav>
      </Navbar>
      <Switch>
        <Route path="/home">
            <Home />
        </Route>
        <Route path="/about">
            <About />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/article">
            <Article />
          </Route>
        </Switch>
    </div> 
  );
};

export default Components;
