import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Row, Col } from 'reactstrap';
import image2 from "./assests/typo.jpg";
  
  const Article = () => {
    return (
      <div>
        <Row>
            <Col md="9">
                <h2>Tips Menghilangkan Kebiasaan Typo</h2>
                <img className="image" src={image2} alt="medium"/>
                <p>Halo, coretan singkat kali ini akan membahas tentang Bagaimana mengurangi ke-typo-an.
                Kesalahan tipografi atau galat tipografi (dalam bahasa Inggris biasa disingkat typo) adalah kesalahan yang dibuat pada saat proses mengetik. … Typo dapat mengubah arti kata atau bahkan arti dari kalimat, terutama dalam bahasa Indonesia
                Pokoknya typo itu Salah ketik deh, ibarat kamu mau mengetik ‘’baju‘’ eh malah terketik ‘’vaju” typo sendiri mungkin diakibatkan karena kita memiliki Hp touch screen. Memang memakai keyboard fisik seperti Blackberry bisa mengurangi typo tapi bukan berarti Kita terbebas dari typo ini selamanya karena bisa saja orang yang memakai keyboard fisik pun terjangkit typo jika mereka tidak fokus.
                Jujur, saya sendiri juga salah satu yang sering typo jika menulis, apalagi saat saya menulis sebuah cerpen atau puisi, saya akan sangat kewalahan Karena tidak selamanya mata saya terfokus pada layar Hp. Rasanya kesel banget kalau harus edit satu-satu.
                Banyak faktor yang bisa membuat kita typo salah satunya karena jari jempol kita lebih besar daripada keyboard yang ada pada layar HP kita hehe
                Kemarin saya searching di internet dan membaca bagaimana cara mengurangi typo. Sebenarnya sudah sangat banyak cara-caranya, maka dari itu saya akan menambah tiga lagi tips Bagaimana mengurangi typo versi saya.
                Langkah pertama mungkin kita harus membesarkan ukuran layar keyboard kita. Semakin besar layar keyboard, maka semakin kecil pula kemungkinan kita typo. Tapi memang biasanya layar keyboard yang besar akan sangat mengganggu jika kita memiliki hp dengan layar dibawah 4inch. Kalau sudah seperti itu maka saya sarankan kalian untuk meng instal keyboard yang baru.
                Saya sendiri menggunakan keyboard SwiftKey. Keyboardnya terbilang sangat simple tapi tidak tahu mengapa saya sangat nyaman memakainya. Keyboardnya terbilang canggih bahkan saat saya baru mengetik dua huruf, dia sudah memprediksikan kata apa yang ingin saya ketik. Magic. Keyboard ini juga secara otomatis membenarkan kata yang salah. Keyboard ini pribadi sangat berguna bagi penulis seperti saya yang intens mengetik di layar hp. Jadi bagi kalian yang memiliki layar hp yang kecil, keyboard ini akan sangat membantu kalian juga. Fungsi lain yang menurut saya berguna adalah, saat kita mengetik tanda titik(.) keyboard secara otomatis memberi spasi, dan memulai kata selanjutnya dengan huruf kapital. Gimana? Keren? 
                Kedua, usahakan keyboard kita tidak menggunakan tema apapun atau polos. Memang sih keren jika keyboard kita ada fotonya. Tapi itu akan mengurangi fokus kita dalam mengetik. Warna latar tema akan membuat huruf-huruf pada keyboard menjadi tidak jelas. Sebaiknya gunakan tema yang simple yang tidak memiliki terlalu banyak motif. Saya sendiri menggunakan tema polos berwarna hitam, karena selain enak di pandang pun kalau saya ngetik di ruangan gelap, mata saya tidak akan begitu perih.
                Ketiga, sebaiknya keyboard kita tidak insert nomor, selain unfaedah pun keyboard akan terlihat kecil. Sebaiknya hilangkan. Jika kita ingin mengetik angka, ketik saja pada tombol 123 yang biasanya ada pada sudut kiri keyboard kita.
                </p>
            </Col>
        </Row>
    </div> 
    );
  };

export default Article;

