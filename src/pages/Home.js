import React from "react";
import{ Jumbotron, Button } from "reactstrap";

const Home = (props) => {
  return(
      <div>
          <Jumbotron>
              <h1 className="display-3">My Site</h1>
              <p className="lead">I hope you can finde what are you looking for here</p>
              <hr className="my-2" />
              <p>(about me ofcourse)</p>
              <p className="lead">
                <Button color="primary" href="/about/">Click here to find more!</Button>
              </p>
          </Jumbotron>
      </div>
  );
};

export default Home;
