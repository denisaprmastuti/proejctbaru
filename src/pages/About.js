import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import image3 from "./assests/padp.jpg";
import {
    Card, 
    CardImg, 
    CardTitle, 
    CardText, 
    CardDeck, 
    CardBody
} from "reactstrap";

const About = (prop) => {
  return (
      <CardDeck>
      <Card>
        <CardImg className="image" src={image3} alt="Card image cap" />
        <CardBody>
            <CardTitle tag="h5">About Me</CardTitle>
            <CardText>
            Name: Putri Ayu Denisa Pramastuty
            <br/>
            DoB: 23 July 1996
            <br/>
            Occupation: Student's at Glints Academy Bootcamp
            </CardText>
        </CardBody>
      </Card>
    </CardDeck>
  )
};

export default About;